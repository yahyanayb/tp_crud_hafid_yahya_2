<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\todoController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[todoController::class,'index']);
Route::post('/new',[todoController::class,'new']);
Route::delete('/delete/{id}',[todoController::class,'delete']);
Route::post('/toggle/{id}',[todoController::class,'toggle']);
