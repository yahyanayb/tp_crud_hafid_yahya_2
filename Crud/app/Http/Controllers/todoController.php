<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Todo;

class todoController extends Controller
{
    public function index()
    {
        $tasks = Todo::all();
        return view("welcome",compact('tasks'));
    }
    public function new(Request $request)
    {
        $request->validate([
            'title' => 'required',
        ]);
        Todo::create($request->all());
        return redirect('/');
    }
    public function delete($id)
    {
        $tasks = Todo::find($id);
        $tasks->delete();
        return redirect('/');
    }
    public function toggle($id)
    {
        $tasks = Todo::find($id);
        $tasks->done = !$tasks->done;
        $tasks->save();
        return redirect('/');
    }
}
