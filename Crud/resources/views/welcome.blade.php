<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
  </head>
  <body>
    <form action="{{url('/new')}}" method="POST">
        @csrf
    <nav class="navbar bg-dark border-bottom border-body" data-bs-theme="dark">
      <div class="container-fluid">
        <h5 class="navbar-brand text-white">TodoList</h5>
      </div>
    </nav>
    <div class="container mt-5 text-center">
      <div class="row justify-content-center">
        <div class="col-md-6">
          <div class="input-group">
            <div class="form-outline">
              <input type="text" name="title" placeholder="Task Title" class="form-control" />
            </div>
            <button type="submit" class="btn btn-primary" data-mdb-ripple-init>
              Add
            </button>
          </div>
        </div>
      </div>
    </div>
    </form>
    @foreach ($tasks as $task)
    <div class="container mt-5">
      <div class="alert  {{$task->done ? 'alert-success' : 'alert-warning'}}  d-flex align-items-center" role="alert">
        <span style="flex-grow: 1;">
          {{$task->title}}
        </span>
        <form action="{{ url('/toggle',['id' => $task->id])}}" method="POST">
            @csrf
        <button type="submit" class="btn btn-primary" data-mdb-ripple-init>
            {{$task->done ? "undo" : "done"}}
        </button>
        </form>
        <form action="{{ url('/delete',['id' => $task->id])}}" method="POST">
            @csrf
            @method('delete')
        <button type="submit" class="btn btn-danger ms-1" data-mdb-ripple-init>
          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-lg" viewBox="0 0 16 16">
            <path d="M2.146 2.854a.5.5 0 1 1 .708-.708L8 7.293l5.146-5.147a.5.5 0 0 1 .708.708L8.707 8l5.147 5.146a.5.5 0 0 1-.708.708L8 8.707l-5.146 5.147a.5.5 0 0 1-.708-.708L7.293 8z"/>
          </svg>
        </button>
        </form>
      </div>
      </div>
      @endforeach
    </div>
  </body>
</html>